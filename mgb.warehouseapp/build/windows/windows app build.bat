@echo off
:ask-release-or-debug
echo Press 'd' to build debug, 'r' to build release
set /p dorr=(d/r)
@echo on



CALL cordova create mgb.warehouseapp
mkdir release
copy ..\..\mgb.warehouseapp\www mgb.warehouseapp\
cd mgb.warehouseapp 
CALL cordova platform add android
CALL cordova plugin add ..\..\cordova-plugin-ChangeRfid

If %dorr%==d goto debug
If %dorr%==r goto release
If %dorr%==D goto debug
If %dorr%==R goto release


:debug
CALL cordova build --debug
cd ..
mkdir release\debug
copy mgb.warehouseapp\platforms\android\build\outputs\apk\* release\debug

:release
CALL cordova build --release
cd .. 
mkdir release\release
copy mgb.warehouseapp\platforms\android\build\outputs\apk\* release\release