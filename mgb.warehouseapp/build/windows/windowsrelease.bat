@echo off
:ask-release-or-debug
echo Press 'd' to build debug, 'r' to build release
set /p dorr=(d/r)
@echo on

mkdir release
cd ..\..\mgb.warehouseapp
CALL cordova plugin remove com.matd.chgangerfid
CALL cordova plugin add ..\cordova-plugin-ChangeRfid 

If %dorr%==d goto debug
If %dorr%==r goto release
If %dorr%==D goto debug
If %dorr%==R goto release


:debug
CALL cordova build --debug
cd ..\build\windows\release
mkdir release\debug
copy ..\..\..\mgb.warehouseapp\platforms\android\build\outputs\apk\* release\debug