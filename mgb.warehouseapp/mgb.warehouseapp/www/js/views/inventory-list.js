﻿/// <reference path="/js/plugins/jquery-2.0.2.min.js" />
/// <reference path="/js/config.js" />
/// <reference path="/js/helpers.js" />
/// <reference path="/js/api/mgb-api.js" />
/// <reference path="/js/api/rfid-scanner.js" />

// Code relating to the Equipment page (the homepage)
inventoryList = {
	equipmentData: null,
	HasInit: false,

	UI: {
		container: '#inventoryList'
	},

	init: function () {
		this.header.init();
		this.hasInit = true;
	},

	load: function (equipmentData) {
		this.equipmentData = equipmentData;

		if (!this.HasInit) this.init();

		this.header.setHtml();
		this.list.load();
		$(this.UI.container).addClass('show');
	},

	header: {
		UI: {
			img: '#inventoryList > .gryPnl img',
			rightPanel: '#inventoryList > .gryPnl .right',
			back: '#inventoryList > .gryPnl .back',
			name: '#inventoryList > .gryPnl .name',
			finish: '#inventoryList > .gryPnl .finish',
		},

		init: function () {
			this.setupHandlers();
		},

		setHtml: function () {
			$(this.UI.img).attr('src', config.mgbApi.url + inventoryList.equipmentData.imgUrl).show();
			$(this.UI.name).html(inventoryList.equipmentData.name);
			if (inventoryList.equipmentData.finish === '') {
				$(this.UI.finish).html('');
			} else {
				$(this.UI.finish).html('Finish: ' + inventoryList.equipmentData.finish);
			}
		},

		setupHandlers: function () {
			$(this.UI.back).click(function () {
				pageLoader(pageEnum.EQUIPMENTLIST);
			});

			$(this.UI.img).on('error', function () {
				$(this).hide();
			});
		}
	},

	list: {
		HasInit: false,
		data: null,

		UI: {
			container: '#inventoryList > .cntList',
			table: '#inventoryList > .cntList > table',
			instructions: '#inventoryList > .cntList > p'
		},

		init: function () {
			this.setupHandlers();
		},

		load: function () {
			if (!this.HasInit) this.init();

			mgbApi.getInventory(inventoryList.equipmentData.id, function (data) {
				inventoryList.list.setData(data);
			});
		},

		setupHandlers: function () {
			$(this.UI.table).click('tr', function (event) {
				var target = $(event.target);
				var tr = target.closest('tr');
				var i = tr.attr('data-i');
				if (typeof i !== 'undefined') {
					var inventory = inventoryList.list.data[i];

					inventoryList.rfidPopup.load(inventory.id, inventory.rfid);
				}
			});
		},

		// Set equipment table with data array
		setData: function (data) {
			// Removing old data
			$(this.UI.table).find('tr:not(.head)').remove();

			if (data.length === 0) {
				$(this.UI.instructions).html('No inventory');
				$(this.UI.table).removeClass('show');
			} else {
				// Adding new data
				var html = '';

				for (var i = 0; i < data.length; i++) {
					var parsedData = data[i];
					var inventory = {
						id: parsedData.inventoryId,
						serialNumber: parsedData.serialNum === null ? '' : parsedData.serialNum,
						rfid: parsedData.rfid === null ? '' : parsedData.rfid,
						quality: parsedData.quality
					}

					data[i] = inventory;

					html += '<tr data-i="' + i + '">';

					// serial number
					html += '<td>' + inventory.serialNumber + '</td>';

					// rfid
					html += '<td>' + inventory.rfid + '</td>';

					// quality
					html += '<td>' + (inventory.quality ? 'Good' : 'Poor') + '</td>'; //Always returns true?

					html += '</tr>';
				}

				this.Data = data;

				$(this.UI.Table).children('tbody').append(html);
				$(this.UI.Table).addClass('show');

				$(this.UI.instructions).html('Tap an Inventory row to modify its RFID');
			}

			$(this.UI.container).addClass('show');
		},
	},

	rfidPopup: {
		HasInit: false,

		inventoryData: {
			id: null,
			rfid: null
		},

		UI: {
			mask: '#inventoryList > .rfidPopupMask',
			panel: '#inventoryList .rfidPopup',
			currentRFID: '#inventoryList .rfidPopup .currentRFID',
			newRFID: '#inventoryList .rfidPopup .newRFID',
			writeRFID: '#inventoryList .rfidPopup .writeRFID',
			cancel: '#inventoryList .rfidPopup .cancel',
			status: '#inventoryList .rfidPopup .status'
		},

		init: function () {
			this.setupHandlers();
			this.HasInit = true;
		},

		load: function (id, rfid) {
			if (!this.HasInit) this.init();
			this.inventoryData.id = id;
			this.inventoryData.rfid = rfid;

			this.setupHTML();
			$(inventoryList.rfidPopup.UI.mask).addClass('show');
		},

		setupHandlers: function () {
			$(this.UI.writeRFID).click(function () {
				$(inventoryList.rfidPopup.UI.status).html('Writing RFID...');
				var epc = $(inventoryList.rfidPopup.UI.newRFID).val();
				rfidScanner.writeEpc(epc, function (data) { 
					// success
					$(inventoryList.rfidPopup.UI.status).html('RFID written, now saving');
					mgbApi.updateInventory(inventoryList.rfidPopup.inventoryData.id, epc, function (data) {
						$(inventoryList.rfidPopup.UI.status).html('RFID Sent through');
						if (data[0] === "1") {
							$(inventoryList.rfidPopup.UI.status).html('RFID saved');
							$(inventoryList.rfidPopup.UI.cancel).html('Close');
						} else {
							var msg = 'Error: ' + data[1];
							$(inventoryList.rfidPopup.UI.status).html(msg);
						}
					});
				}, function (data) {
					// failure
					$(inventoryList.rfidPopup.UI.status).html('Failure: ' + data);
				});
			});

			$(this.UI.cancel).click(function () {
				$(inventoryList.rfidPopup.UI.mask).removeClass('show');
				$(inventoryList.rfidPopup.UI.status).html('');
				$(inventoryList.rfidPopup.UI.cancel).html('Cancel');
				InventoryList.list.load();
			});
		},

		setupHTML: function () {
			if (this.inventoryData.rfid === '') {
				$(this.UI.currentRFID).val('[blank]');
			} else {
				$(this.UI.currentRFID).val(this.inventoryData.rfid);
			}

			var hexTimestamp = padZeroes(unixTimestamp().toString(16), 9);
			var newRFID = config.appSerial + hexTimestamp + padZeroes(this.inventoryData.id, 6);
			$(this.UI.newRFID).val(newRFID);
		}
	}
}