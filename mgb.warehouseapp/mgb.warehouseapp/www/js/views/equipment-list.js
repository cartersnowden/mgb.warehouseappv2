﻿/// <reference path="/js/plugins/jquery-2.0.2.min.js" />
/// <reference path="/js/config.js" />
/// <reference path="/js/helpers.js" />
/// <reference path="/js/api/mgb-api.js" />
/// <reference path="/js/views/inventory-list.js" />

// Code relating to the Equipment page (the homepage)
equipmentList = {
	hasInit: false,

	UI: {
		container: '#equipmentList'
	},

	load: function () {
		if (!this.HasInit) this.init();

		$(this.UI.container).addClass('show');
	},

	init: function () {
		this.searchPanel.init();
		this.list.init();
		this.HasInit = true;
	},

	// Properties and methods of the page's drop downs
	searchPanel: {
		UI: {
			filters: '#equipmentList .gryPnl select, #equipmentList .keywords',
			brand: '#equipmentList .brand',
			equipmentType: '#equipmentList .eqType',
			keywords: '#equipmentList .keywords'
			//Search: '#search'
		},

		init: function () {
			this.setupHtml();
			this.setupHandlers();
		},

		setupHtml: function () {
			// Filling Brand drop down
			mgbApi.getBrands(function (data) {
				var html = '<option value="0">All brands</option>';
				for (var i = 0; i < data.length; i++) {
					var brand = data[i];
					html += '<option value="' + brand.brandId + '">' + brand.brandName + '</option>';
				}

				$(equipmentList.searchPanel.UI.brand).html(html);
			});

			// Filling EquipmentType drop down
			mgbApi.getEquipmentTypes(function (data) {
				var html = '<option value="0">All equipment types</option>';
				for (var i = 0; i < data.length; i++) {
					var equipmentType = data[i];
					html += '<option value="' + equipmentType.equipmentTypeId + '">' + equipmentType.name + '</option>';
				}

				$(equipmentList.searchPanel.UI.equipmentType).html(html);
			});
		},

		setupHandlers: function () {
			$(this.UI.filters).change(function () {
				equipmentList.searchPanel.search();
			});

			$(this.UI.keywords).keypress(function (event) {
				if (event.which === 13) $(this).blur();
			});
		},

		search: function () {
			var brandID = $(this.UI.brand).children(':selected').val();
			if (parseInt(brandID) <= 0) brandID = null;

			var equipmentTypeID = $(this.UI.equipmentType).children(':selected').val();
			if (parseInt(equipmentTypeID) <= 0) equipmentTypeID = null;

			var strKeywords = $(this.UI.keywords).val();
			var keywords = strKeywords.split(' ');
			if (strKeywords.length === 0) keywords = null;

			mgbApi.getEquipment(brandID, equipmentTypeID, keywords, function (data) {
				equipmentList.list.setData(data);
			});
		}
	},

	list: {
		data: null,

		UI: {
			container: '#equipmentList > .cntList',
			table: '#equipmentList > .cntList > table',
			instructions: '#equipmentList > .cntList > p',
			showMore: '#equipmentList > .cntList .showMore'
		},

		init: function () {
			this.setupHandlers();
		},

		setupHandlers: function () {
			$(this.UI.table).click('tr', function (event) {
				var target = $(event.target);
				var tr = target.closest('tr');
				var i = tr.attr('data-i');

				if (typeof i !== 'undefined') {
					var equipment = equipmentList.list.data[i];

					pageLoader(pageEnum.INVENTORYLIST, equipment);
				}
			});

			$(this.UI.showMore).click(function () {
				equipmentList.list.showMoreRows();
			});
		},

		// Set equipment table with data array
		setData: function (data) {
			// Removing old data
			$(this.UI.table).find('tr:not(.head)').remove();

			if (data.length === 0) {
				$(this.UI.instructions).html('No results');
				$(this.UI.table).removeClass('show');
				$(this.UI.showMore).removeClass('show');
			} else {
				// Adding new data
				var html = '';

				for (var i = 0; i < data.length; i++) {
					var parsedData = data[i];
					var equipment = {
						id: parsedData.equipmentId,
						name: parsedData.name,
						imgUrl: parsedData.imageFileName,
						finish: parsedData.colorFinish === null ? '' : parsedData.colorFinish
					}

					data[i] = equipment;

					html += '<tr data-i="' + i + '" class="hide">';

					// image
					var imgUrl = config.mgbApi.url + equipment.imgUrl
					html += '<td><img src="" data-src="' + imgUrl + '" width="60" height="60" alt="equipment image" /></td>';

					// name
					html += '<td>' + equipment.name + '</td>';

					// finish
					html += '<td>' + equipment.finish + '</td>';

					html += '</tr>';
				}

				this.data = data;

				$(this.UI.table).children('tbody').append(html);

				$(this.UI.table).find('img').on('error', function () {
					if (this.offsetParent !== null) {
						$(this).remove();
					}
				});

				$(this.UI.table).addClass('show');
				$(this.UI.showMore).addClass('show');
				this.showMoreRows();

				$(this.UI.instructions).html('Tap an Equipment row to see its inventory');
			}

			$(this.UI.container).addClass('show');
		},

		showMoreRows: function () {
			$(this.UI.table).find('tr.hide').slice(0, 20).each(function () {
				$(this).removeClass('hide');
				var img = $(this).find('img');
				img.attr('src', img.attr('data-src'));
			});

			var countStillHidden = $(this.UI.table).find('tr.hide').length;

			if (countStillHidden === 0) {
				$(this.UI.showMore).removeClass('show');
			} else if (countStillHidden < 3) {
				this.showMoreRows();
			}
		}
	}
};