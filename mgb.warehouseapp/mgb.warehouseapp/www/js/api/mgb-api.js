﻿/// <reference path="/js/config.js" />
/// <reference path="/js/helpers.js" />

// Controller to utilise MGB API
mgbApi = {
	// Standard AJAX function
	ajaxRequest: function (type, path, data, onSuccess) {
		$.ajax({
			type: type,
			url: config.mgbApi.url + path,
			dataType: 'json',
			data: data,
			async: false,
			//Send Authorization header in Base64
			beforeSend: function (xhr) {
				xhr.setRequestHeader("Authorization", "Basic " + window.btoa(config.mgbApi.username + ":" + config.mgbApi.password));
			},
			success: function (data) {
				onSuccess(data);
			}
		});
	},

	// Gets list of all Brands
	getBrands: function (onSuccess) {
		this.ajaxRequest("GET", "/brands", null, onSuccess);
	},

	// Gets list of all Equipment Types
	getEquipmentTypes: function (onSuccess) {
		this.ajaxRequest("GET", "/equipment-types", null, onSuccess);
	},

	// Gets selection of equipment. string brandID, string equipmentTypeID, string[] keywords)
	getEquipment: function (brandID, equipmentTypeID, keywords, onSuccess) {
		// Prepping parameters
		var data = {};

		if (brandID !== null) data.BrandID = brandID;
		if (equipmentTypeID !== null) data.EquipmentTypeID = equipmentTypeID;
		if (keywords !== null && typeof keywords === "object") {
			var strKeywords = keywords[0];

			for (var i = 1; i < keywords.length; i++) {
				strKeywords += "+" + keywords[i];
			}

			data.Keywords = strKeywords;
		}

		// Requesting Equipment list
		this.ajaxRequest("GET", "/equipment", data, onSuccess);
	},

	// Gets list of inventory with a specific EquipmentID
	getInventory: function (equipmentID, onSuccess) {
		this.ajaxRequest("GET", "/equipment/" + equipmentID + "/inventories", null, onSuccess);
	},

	// Updates information for a piece of inventory
	updateInventory: function (inventoryID, rfid, onSuccess) {
		var data = {};
		if (rfid !== null) data.rfid = rfid;

		this.ajaxRequest("PUT", "/inventories/" + inventoryID, data, onSuccess);
	}
};