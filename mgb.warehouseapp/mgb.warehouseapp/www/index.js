/// <reference path="/helpers.js" />

/// <reference path="/js/api/mgb-api.js" />
/// <reference path="/js/api/rfid-scanner.js" />

/// <reference path="/js/views/inventory-list.js" />
/// <reference path="/js/views/equipment-list.js" />


app = {
	initialize: function () {
		this.bindEvents();
	},

	bindEvents: function () {
		document.addEventListener('deviceready', this.onDeviceReady, false);
	},

	onDeviceReady: function () {
		app.receivedEvent('deviceready');
	},

	receivedEvent: function (id) {
		pageLoader(pageEnum.EQUIPMENTLIST, null);
	}
};
