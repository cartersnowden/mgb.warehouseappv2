﻿/// <reference path="/js/config.js" />
/// <reference path="/js/helpers.js" />

// Controller to utilise MGB API
RfidScanner = {
	// Writes EPC to an RFID tag
	WriteEPC: function (epc, onSuccess, onFailure) {
		cordova.exec(onSuccess, onFailure, "EPCWriter", "writeEPC", [epc]);
	}
}