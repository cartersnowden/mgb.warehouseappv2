﻿/// <reference path="/js/config.js" />
/// <reference path="/js/helpers.js" />

// Controller to utilise MGB API
MgbApi = {
	// Standard AJAX function
	AjaxRequest: function (type, path, data, onSuccess) {
		$.ajax({
			type: type,
			url: Config.MgbApi.Url + path,
			dataType: 'json',
			data: data,
			async: false,
			//Send Authorization header in Base64
			beforeSend: function (xhr) {
				xhr.setRequestHeader("Authorization", "Basic " + window.btoa(Config.MgbApi.Username + ":" + Config.MgbApi.Password));
			},
			success: function (data) {
				onSuccess(data);
			}
		});
	},

	// Gets list of all Brands
	GetBrands: function (onSuccess) {
		this.AjaxRequest("GET", "/brands", null, onSuccess);
	},

	// Gets list of all Equipment Types
	GetEquipmentTypes: function (onSuccess) {
		this.AjaxRequest("GET", "/equipment-types", null, onSuccess);
	},

	// Gets selection of equipment. string brandID, string equipmentTypeID, string[] keywords)
	GetEquipment: function (brandID, equipmentTypeID, keywords, onSuccess) {
		// Prepping parameters
		var data = {};

		if (brandID !== null) data.BrandID = brandID;
		if (equipmentTypeID !== null) data.EquipmentTypeID = equipmentTypeID;
		if (keywords !== null && typeof keywords === "object") {
			var strKeywords = keywords[0];

			for (var i = 1; i < keywords.length; i++) {
				strKeywords += "+" + keywords[i];
			}

			data.Keywords = strKeywords;
		}

		// Requesting Equipment list
		this.AjaxRequest("GET", "/equipment", data, onSuccess);
	},

	// Gets list of inventory with a specific EquipmentID
	GetInventory: function (equipmentID, onSuccess) {
		this.AjaxRequest("GET", "/equipment/" + equipmentID + "/inventories", null, onSuccess);
	},

	// Updates information for a piece of inventory
	UpdateInventory: function (inventoryID, rfid, onSuccess) {
		var data = {};
		if (rfid !== null) data.rfid = rfid;

		this.AjaxRequest("POST", "/inventories/" + inventoryID, data, onSuccess);
	}
}