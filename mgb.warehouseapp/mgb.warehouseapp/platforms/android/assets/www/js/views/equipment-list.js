﻿/// <reference path="/js/plugins/jquery-2.0.2.min.js" />
/// <reference path="/js/config.js" />
/// <reference path="/js/helpers.js" />
/// <reference path="/js/api/mgb-api.js" />
/// <reference path="/js/views/inventory-list.js" />

// Code relating to the Equipment page (the homepage)
EquipmentList = {
	HasInit: false,

	UI: {
		Container: '#equipmentList'
	},

	Load: function () {
		if (!this.HasInit) this.Init();

		$(this.UI.Container).addClass('show');
	},

	Init: function () {
		this.SearchPanel.Init();
		this.List.Init();
		this.HasInit = true;
	},

	// Properties and methods of the page's drop downs
	SearchPanel: {
		UI: {
			Filters: '#equipmentList .gryPnl select, #equipmentList .keywords',
			Brand: '#equipmentList .brand',
			EquipmentType: '#equipmentList .eqType',
			Keywords: '#equipmentList .keywords'
			//Search: '#search'
		},

		Init: function () {
			this.SetupHtml();
			this.SetupHandlers();
		},

		SetupHtml: function () {
			// Filling Brand drop down
			MgbApi.GetBrands(function (data) {
				var html = '<option value="0">All brands</option>';
				for (var i = 0; i < data.length; i++) {
					var brand = data[i];
					html += '<option value="' + brand.brandId + '">' + brand.brandName + '</option>';
				}

				$(EquipmentList.SearchPanel.UI.Brand).html(html);
			});

			// Filling EquipmentType drop down
			MgbApi.GetEquipmentTypes(function (data) {
				var html = '<option value="0">All equipment types</option>';
				for (var i = 0; i < data.length; i++) {
					var equipmentType = data[i];
					html += '<option value="' + equipmentType.equipmentTypeId + '">' + equipmentType.name + '</option>';
				}

				$(EquipmentList.SearchPanel.UI.EquipmentType).html(html);
			});
		},

		SetupHandlers: function () {
			$(this.UI.Filters).change(function () {
				EquipmentList.SearchPanel.Search();
			});

			$(this.UI.Keywords).keypress(function (event) {
				if (event.which === 13) $(this).blur();
			});
		},

		Search: function () {
			var brandID = $(this.UI.Brand).children(':selected').val();
			if (parseInt(brandID) <= 0) brandID = null;

			var equipmentTypeID = $(this.UI.EquipmentType).children(':selected').val();
			if (parseInt(equipmentTypeID) <= 0) equipmentTypeID = null;

			var strKeywords = $(this.UI.Keywords).val();
			var keywords = strKeywords.split(' ');
			if (strKeywords.length === 0) keywords = null;

			MgbApi.GetEquipment(brandID, equipmentTypeID, keywords, function (data) {
				EquipmentList.List.SetData(data);
			});
		}
	},

	List: {
		Data: null,

		UI: {
			Container: '#equipmentList > .cntList',
			Table: '#equipmentList > .cntList > table',
			Instructions: '#equipmentList > .cntList > p',
			ShowMore: '#equipmentList > .cntList .showMore'
		},

		Init: function () {
			this.SetupHandlers();
		},

		SetupHandlers: function () {
			$(this.UI.Table).click('tr', function (event) {
				var target = $(event.target);
				var tr = target.closest('tr');
				var i = tr.attr('data-i');

				if (typeof i !== 'undefined') {
					var equipment = EquipmentList.List.Data[i];

					pageLoader(pageEnum.INVENTORYLIST, equipment);
				}
			});

			$(this.UI.ShowMore).click(function () {
				EquipmentList.List.ShowMoreRows();
			});
		},

		// Set equipment table with data array
		SetData: function (data) {
			// Removing old data
			$(this.UI.Table).find('tr:not(.head)').remove();

			if (data.length === 0) {
				$(this.UI.Instructions).html('No results');
				$(this.UI.Table).removeClass('show');
				$(this.UI.ShowMore).removeClass('show');
			} else {
				// Adding new data
				var html = '';

				for (var i = 0; i < data.length; i++) {
					var parsedData = data[i];
					var equipment = {
						id: parsedData.equipmentId,
						name: parsedData.name,
						imgUrl: parsedData.imageFileName,
						finish: parsedData.colorFinish === null ? '' : parsedData.colorFinish
					}

					data[i] = equipment;

					html += '<tr data-i="' + i + '" class="hide">';

					// image
					var imgUrl = Config.MgbApi.Url + equipment.imgUrl
					html += '<td><img src="" data-src="' + imgUrl + '" width="60" height="60" alt="equipment image" /></td>';

					// name
					html += '<td>' + equipment.name + '</td>';

					// finish
					html += '<td>' + equipment.finish + '</td>';

					html += '</tr>';
				}

				this.Data = data;

				$(this.UI.Table).children('tbody').append(html);

				$(this.UI.Table).find('img').on('error', function () {
					if (this.offsetParent !== null) {
						$(this).remove();
					}
				});

				$(this.UI.Table).addClass('show');
				$(this.UI.ShowMore).addClass('show');
				this.ShowMoreRows();

				$(this.UI.Instructions).html('Tap an Equipment row to see its inventory');
			}

			$(this.UI.Container).addClass('show');
		},

		ShowMoreRows: function () {
			$(this.UI.Table).find('tr.hide').slice(0, 20).each(function () {
				$(this).removeClass('hide');
				var img = $(this).find('img');
				img.attr('src', img.attr('data-src'));
			});

			var countStillHidden = $(this.UI.Table).find('tr.hide').length;

			if (countStillHidden === 0) {
				$(this.UI.ShowMore).removeClass('show');
			} else if (countStillHidden < 3) {
				this.ShowMoreRows();
			}
		}
	}
};