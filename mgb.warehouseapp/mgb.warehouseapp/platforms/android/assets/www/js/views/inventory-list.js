﻿/// <reference path="/js/plugins/jquery-2.0.2.min.js" />
/// <reference path="/js/config.js" />
/// <reference path="/js/helpers.js" />
/// <reference path="/js/api/mgb-api.js" />
/// <reference path="/js/api/rfid-scanner.js" />

// Code relating to the Equipment page (the homepage)
InventoryList = {
	EquipmentData: null,
	HasInit: false,

	UI: {
		Container: '#inventoryList'
	},

	Init: function () {
		this.Header.Init();
		this.HasInit = true;
	},

	Load: function (equipmentData) {
		this.EquipmentData = equipmentData;

		if (!this.HasInit) this.Init();

		this.Header.SetHtml();
		this.List.Load();
		$(this.UI.Container).addClass('show');
	},

	Header: {
		UI: {
			Img: '#inventoryList > .gryPnl img',
			RightPanel: '#inventoryList > .gryPnl .right',
			Back: '#inventoryList > .gryPnl .back',
			Name: '#inventoryList > .gryPnl .name',
			Finish: '#inventoryList > .gryPnl .finish',
		},

		Init: function () {
			this.SetupHandlers();
		},

		SetHtml: function () {
			$(this.UI.Img).attr('src', Config.MgbApi.Url + InventoryList.EquipmentData.imgUrl).show();
			$(this.UI.Name).html(InventoryList.EquipmentData.name);
			if (InventoryList.EquipmentData.finish === '') {
				$(this.UI.Finish).html('');
			} else {
				$(this.UI.Finish).html('Finish: ' + InventoryList.EquipmentData.finish);
			}
		},

		SetupHandlers: function () {
			$(this.UI.Back).click(function () {
				pageLoader(pageEnum.EQUIPMENTLIST);
			});

			$(this.UI.Img).on('error', function () {
				$(this).hide();
			});
		}
	},

	List: {
		HasInit: false,
		Data: null,

		UI: {
			Container: '#inventoryList > .cntList',
			Table: '#inventoryList > .cntList > table',
			Instructions: '#inventoryList > .cntList > p'
		},

		Init: function () {
			this.SetupHandlers();
		},

		Load: function () {
			if (!this.HasInit) this.Init();

			MgbApi.GetInventory(InventoryList.EquipmentData.id, function (data) {
				InventoryList.List.SetData(data);
			});
		},

		SetupHandlers: function () {
			$(this.UI.Table).click('tr', function (event) {
				var target = $(event.target);
				var tr = target.closest('tr');
				var i = tr.attr('data-i');
				if (typeof i !== 'undefined') {
					var inventory = InventoryList.List.Data[i];

					InventoryList.RfidPopup.Load(inventory.id, inventory.rfid);
				}
			});
		},

		// Set equipment table with data array
		SetData: function (data) {
			// Removing old data
			$(this.UI.Table).find('tr:not(.head)').remove();

			if (data.length === 0) {
				$(this.UI.Instructions).html('No inventory');
				$(this.UI.Table).removeClass('show');
			} else {
				// Adding new data
				var html = '';

				for (var i = 0; i < data.length; i++) {
					var parsedData = data[i];
					var inventory = {
						id: parsedData.inventoryId,
						serialNumber: parsedData.serialNum === null ? '' : parsedData.serialNum,
						rfid: parsedData.rfid === null ? '' : parsedData.rfid,
						quality: parsedData.quality
					}

					data[i] = inventory;

					html += '<tr data-i="' + i + '">';

					// serial number
					html += '<td>' + inventory.serialNumber + '</td>';

					// rfid
					html += '<td>' + inventory.rfid + '</td>';

					// quality
					html += '<td>' + (inventory.quality ? 'Good' : 'Poor') + '</td>'; //Always returns true?

					html += '</tr>';
				}

				this.Data = data;

				$(this.UI.Table).children('tbody').append(html);
				$(this.UI.Table).addClass('show');

				$(this.UI.Instructions).html('Tap an Inventory row to modify its RFID');
			}

			$(this.UI.Container).addClass('show');
		},
	},

	RfidPopup: {
		HasInit: false,

		InventoryData: {
			ID: null,
			RFID: null
		},

		UI: {
			Mask: '#inventoryList > .rfidPopupMask',
			Panel: '#inventoryList .rfidPopup',
			CurrentRFID: '#inventoryList .rfidPopup .currentRFID',
			NewRFID: '#inventoryList .rfidPopup .newRFID',
			WriteRFID: '#inventoryList .rfidPopup .writeRFID',
			Cancel: '#inventoryList .rfidPopup .cancel',
			Status: '#inventoryList .rfidPopup .status'
		},

		Init: function () {
			this.SetupHandlers();
			this.HasInit = true;
		},

		Load: function (ID, RFID) {
			if (!this.HasInit) this.Init();
			this.InventoryData.ID = ID;
			this.InventoryData.RFID = RFID;

			this.SetupHTML();
			$(InventoryList.RfidPopup.UI.Mask).addClass('show');
		},

		SetupHandlers: function () {
			$(this.UI.WriteRFID).click(function () {
				$(InventoryList.RfidPopup.UI.Status).html('Writing RFID...');

				var epc = $(InventoryList.RfidPopup.UI.NewRFID).val();
				//cordova.exec("RfidScanner", "writeEPC", [epc]);
				//RfidScanner.WriteEPC(epc, function (data) {
				myRFIDScanner.startScan(epc, function (data) { 
					// success
					$(InventoryList.RfidPopup.UI.Status).html('RFID written, now saving');
					MgbApi.UpdateInventory(InventoryList.RfidPopup.InventoryData.ID, epc, function (data) {
						$(InventoryList.RfidPopup.UI.Status).html('RFID Sent through');
						if (data[0] === "1") {
							$(InventoryList.RfidPopup.UI.Status).html('RFID saved');
							$(InventoryList.RfidPopup.UI.Cancel).html('Close');
						} else {
							var msg = 'Error: ' + data[1];
							$(InventoryList.RfidPopup.UI.Status).html(msg);
						}
					});
				}, function (data) {
					// failure
					$(InventoryList.RfidPopup.UI.Status).html('Failure: ' + data);
				});
			});

			$(this.UI.Cancel).click(function () {
				$(InventoryList.RfidPopup.UI.Mask).removeClass('show');
				$(InventoryList.RfidPopup.UI.Status).html('');
				$(InventoryList.RfidPopup.UI.Cancel).html('Cancel');
				InventoryList.List.Load();
			});
		},

		SetupHTML: function () {
			if (this.InventoryData.RFID === '') {
				$(this.UI.CurrentRFID).val('[blank]');
			} else {
				$(this.UI.CurrentRFID).val(this.InventoryData.RFID);
			}

			var hexTimestamp = padZeroes(unixTimestamp().toString(16), 9);
			var newRFID = Config.AppSerial + hexTimestamp + padZeroes(this.InventoryData.ID, 6);
			$(this.UI.NewRFID).val(newRFID);
		}
	}
}