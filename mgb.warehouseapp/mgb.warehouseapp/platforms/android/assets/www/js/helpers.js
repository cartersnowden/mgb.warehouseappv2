﻿pageEnum = Object.freeze({
	EQUIPMENTLIST: 0,
	INVENTORYLIST: 1
});

function pageLoader(page, data) {
	$('body > div').removeClass('show');

	switch (page) {
		case pageEnum.EQUIPMENTLIST:
			EquipmentList.Load();
			break;

		case pageEnum.INVENTORYLIST:
			InventoryList.Load(data);
			break;
	}
}

// Returns Unix timestamp (seconds since start of 1970)
function unixTimestamp() {
	return Math.round(new Date().getTime() / 1000);
}

function randomNumericDigits(numberOfDigits) {
	var max = Math.pow(10, numberOfDigits);
	var number = Math.floor(max * Math.random());
	return padZeroes(number, numberOfDigits);
}

function padZeroes(number, length) {
	number = number + '';
	while (number.length < length) number = '0' + number;
	return number;
}