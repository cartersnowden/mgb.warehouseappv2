cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "id": "com.matd.chgangerfid.EPCWriter",
        "file": "plugins/com.matd.chgangerfid/www/EPCWriter.js",
        "pluginId": "com.matd.chgangerfid",
        "clobbers": [
            "EPCWriter"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.2.2",
    "com.matd.chgangerfid": "0.2.12"
};
// BOTTOM OF METADATA
});