cordova.define("com.matd.chgangerfid.EPCWriter", function(require, exports, module) {
/// <reference path="/js/config.js" />
/// <reference path="/js/helpers.js" />

// Controller to utilise MGB API
var exec = require("cordova/exec");
myRFIDScanner = {

	startScan: function (epc, onSuccess, onFailure) {
		exec(onSuccess, onFailure, "EPCWriter", "writeEPC", [epc]);
		alert("Did not update RFID, please check Broadcast Reciever in Plugin.");
	}
}
});
