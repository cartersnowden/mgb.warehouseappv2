﻿/// <reference path="/js/config.js" />
/// <reference path="/js/helpers.js" />

// Controller to utilise MGB API
var exec = require("cordova/exec");
rfidScanner = {

	writeEpc: function (epc, onSuccess, onFailure) {
		exec(onSuccess, onFailure, "EPCWriter", "writeEPC", [epc]);
	}
}